"use strict";

function isNumber(arg) {
  return typeof arg === "number";
}

function isString(arg) {
  return typeof arg === "string";
}

function isBoolean(arg) {
  return typeof arg === "boolean";
}

function isNull(arg) {
  return arg === null;
}

function strictBool(value) {
  if (isBoolean(value) === "boolean") {
    return value;
  }

  switch (value) {
    case "false":
      return false;
    case "true":
      return true;
    case "no":
      return false;
    case "yes":
      return true;
    case "n":
      return false;
    case "y":
      return true;
    case "0":
      return false;
    case "1":
      return true;
    case 0:
      return false;
    case 1:
      return true;
    default: {
      new Error("unrecognized boolean");
    }
  }
}

function omit(object, keysToOmit) {
  return Object.keys(object)
    .filter((option) => !keysToOmit.includes(option))
    .reduce((obj, key) => {
      obj[key] = object[key];
      return obj;
    }, {});
}

function removeUndefinedProperties(obj) {
  for (const key in obj) {
    if (obj[key] === undefined) {
      delete obj[key];
    }
  }
  return obj;
}

exports.strictBool = strictBool;
exports.isNumber = isNumber;
exports.isString = isString;
exports.isBoolean = isBoolean;
exports.isNull = isNull;
