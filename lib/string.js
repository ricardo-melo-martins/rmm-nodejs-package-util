
'use strict';

function capitalizeWords(string) {
    return string.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {
    capitalizeWords = capitalizeWords,
    capitalizeFirstLetter = capitalizeFirstLetter
}

